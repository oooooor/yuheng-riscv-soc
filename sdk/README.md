﻿10:55 2021/12/21

## 测试用例

### 0. 使用说明

1. 进入对应文件夹下，执行 `make` 命令  

2. 编译完成后，会生成 .data 和 .coe 文件，并自动复制到 `/soc/tb` 路径下  

3. 运行 Vivado，并进行 Simulation，可自动加载并运行程序（更新程序后只需要 restart 即可，无需重新打开 Simulation）  

**注意，若使用 BRAM 作为 ITCM，则需要重新综合 BRAM IP**  

### 1. simple

测试加减乘除运算  

![](https://gitee.com/backheart/picgo-image/raw/master/img/20211221112923.png)  

### 2. systick

测试延时函数（内核计数器）  

![](https://gitee.com/backheart/picgo-image/raw/master/img/20211221112938.png)  

### 3. xprintf

测试 xprintf 函数打印功能  

![](https://gitee.com/backheart/picgo-image/raw/master/img/20211221113051.png)  

### 4. timer

测试 timer 中断  

![](https://gitee.com/backheart/picgo-image/raw/master/img/20211221113202.png)  

### 5. uart_txd

测试串口发送功能  

![](https://gitee.com/backheart/picgo-image/raw/master/img/20211221113222.png)  

### 6. uart_rxd

测试串口接收功能（UART2 与 UART3 互联，UART1 用于打印）  

![](https://gitee.com/backheart/picgo-image/raw/master/img/20211221113334.png)  

### 7. rtthread-nano

rtthread-nano 内核版本：3.1.5  

![](https://gitee.com/backheart/picgo-image/raw/master/img/20211221113712.png)  

**注意，由于调度逻辑设计的问题，目前不支持 mdelay 等主动触发的线程切换**  