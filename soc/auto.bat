@echo off

cd %~dp0
set cache_floder=project

if exist %~dp0%cache_floder% (
    echo The floder is exist.
    pause
) else (
    vivado -source create_project.tcl
)

exit
