`timescale 1ns / 1ps
/*
 * Copyright (c) 2020-2021, SERI Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-10-29     Lyons        first version
 */

module pa_core_rtu (
    clk_i,                      // system clock
    rst_n_i,                    // system reset

    reg1_raddr_i,               // rs1 address
    reg2_raddr_i,               // rs2 address

    reg_waddr_i,                // rd address
    reg_waddr_vld_i,            // rd field valid

    reg_wdata_i,                // rd data

    reg1_rdata_o,               // rs1 data
    reg2_rdata_o,               // rs2 data

    csr_mtvec_o,                // register mtvec value
    csr_mepc_o,                 // register mepc value
    csr_mstatus_o,              // register mstatus value

    csr_raddr_i,                // read csr register address

    csr_waddr_i,                // write csr register address
    csr_waddr_vld_i,            // write csr register valid

    csr_wdata_i,                // write csr register data

    csr_rdata_o                 // read csr register data
    );

`include "../pa_chip_param.v"

input                           clk_i;
input                           rst_n_i;

input  [`REG_BUS_WIDTH-1:0]     reg1_raddr_i;
input  [`REG_BUS_WIDTH-1:0]     reg2_raddr_i;

input  [`REG_BUS_WIDTH-1:0]     reg_waddr_i;
input                           reg_waddr_vld_i;

input  [`DATA_BUS_WIDTH-1:0]    reg_wdata_i;

output [`DATA_BUS_WIDTH-1:0]    reg1_rdata_o;
output [`DATA_BUS_WIDTH-1:0]    reg2_rdata_o;

output [`DATA_BUS_WIDTH-1:0]    csr_mtvec_o;
output [`DATA_BUS_WIDTH-1:0]    csr_mepc_o;
output [`DATA_BUS_WIDTH-1:0]    csr_mstatus_o;

input  [`CSR_BUS_WIDTH-1:0]     csr_raddr_i;

input  [`CSR_BUS_WIDTH-1:0]     csr_waddr_i;
input                           csr_waddr_vld_i;

input  [`DATA_BUS_WIDTH-1:0]    csr_wdata_i;

output [`DATA_BUS_WIDTH-1:0]    csr_rdata_o;

wire                            clk_i;
wire                            rst_n_i;

wire [`REG_BUS_WIDTH-1:0]       reg1_raddr_i;
wire [`REG_BUS_WIDTH-1:0]       reg2_raddr_i;

wire [`REG_BUS_WIDTH-1:0]       reg_waddr_i;
wire                            reg_waddr_vld_i;

wire [`DATA_BUS_WIDTH-1:0]      reg_wdata_i;

wire [`DATA_BUS_WIDTH-1:0]      reg1_rdata_o;
wire [`DATA_BUS_WIDTH-1:0]      reg2_rdata_o;

wire [`DATA_BUS_WIDTH-1:0]      csr_mtvec_o;
wire [`DATA_BUS_WIDTH-1:0]      csr_mepc_o;
wire [`DATA_BUS_WIDTH-1:0]      csr_mstatus_o;

wire [`CSR_BUS_WIDTH-1:0]       csr_raddr_i;

wire [`CSR_BUS_WIDTH-1:0]       csr_waddr_i;
wire                            csr_waddr_vld_i;

wire [`DATA_BUS_WIDTH-1:0]      csr_wdata_i;

wire [`DATA_BUS_WIDTH-1:0]      csr_rdata_o;


pa_core_csr u_pa_core_csr (
    .clk_i                      (clk_i),
    .rst_n_i                    (rst_n_i),

    .csr_mtvec_o                (csr_mtvec_o),
    .csr_mepc_o                 (csr_mepc_o),
    .csr_mstatus_o              (csr_mstatus_o),

    .csr_raddr_i                (csr_raddr_i),

    .csr_waddr_i                (csr_waddr_i),
    .csr_waddr_vld_i            (csr_waddr_vld_i),

    .csr_wdata_i                (csr_wdata_i),

    .csr_rdata_o                (csr_rdata_o)
);

pa_core_xreg u_pa_core_xreg (
    .clk_i                      (clk_i),
    .rst_n_i                    (rst_n_i),

    .reg1_raddr_i               (reg1_raddr_i),
    .reg2_raddr_i               (reg2_raddr_i),

    .reg_waddr_i                (reg_waddr_i),
    .reg_waddr_vld_i            (reg_waddr_vld_i),

    .reg_wdata_i                (reg_wdata_i),

    .reg1_rdata_o               (reg1_rdata_o),
    .reg2_rdata_o               (reg2_rdata_o)
);

endmodule