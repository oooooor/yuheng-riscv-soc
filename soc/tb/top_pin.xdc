set_property -dict {PACKAGE_PIN Y18 IOSTANDARD LVCMOS33} [get_ports clk_i]
set_property -dict {PACKAGE_PIN L15 IOSTANDARD LVCMOS33} [get_ports rst_n_i]

set_property -dict {PACKAGE_PIN G15 IOSTANDARD LVCMOS33} [get_ports rxd]
set_property -dict {PACKAGE_PIN G16 IOSTANDARD LVCMOS33} [get_ports txd]